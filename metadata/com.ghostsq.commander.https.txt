Categories:System
License:GPLv3
Web Site:https://sites.google.com/site/ghostcommander1
Source Code:http://sourceforge.net/p/gc-webdav/svn/HEAD/tree/
Issue Tracker:http://sourceforge.net/p/ghostcommander/_list/tickets
Donate:http://sourceforge.net/p/ghostcommander/donate

Auto Name:Ghost Commander - WebDAV plugin
Summary:Access files over WebDAV
Description:
A plug-in for [[com.ghostsq.commander]] to access WebDAV sites. Launch Ghost
Commander and go to 'Menu > Location > Home > WebDAV site'. Alternatively,
scroll along the toolbar until you arrive at 'Home'. Enter your server name and
credentials.
.

Repo Type:git-svn
Repo:https://svn.code.sf.net/p/gc-webdav/svn/

Build:1.0,3
    disable=broken build
    commit=9
    srclibs=GhostCommander@507
    extlibs=jcifs/jcifs-1.3.17.jar,custom_rules.xml
    prebuild=mv libs/custom_rules.xml ./ && \
        pushd $$GhostCommander$$ && \
        android update project -p ./ -t "android-19" && \
        ant debug -f build.xml && \
        jar c -C bin/classes/ com > gc.jar && \
        popd && \
        install -D $$GhostCommander$$/gc.jar libs/gc.jar && \
        android update project -p . -t android-19 -n com.ghostsq.commander.https

Maintainer Notes:
* License?
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.1b2
Current Version Code:5
