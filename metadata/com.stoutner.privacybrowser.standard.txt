Categories:Internet
License:GPLv3+
Author Name:Soren Stoutner
Author Email:soren@stoutner.com
Web Site:https://www.stoutner.com/privacy-browser
Source Code:https://git.stoutner.com/?p=PrivacyBrowser.git;a=summary
Issue Tracker:https://redmine.stoutner.com/projects/privacy-browser/issues
Changelog:https://www.stoutner.com/privacy-browser/changelog/

Auto Name:Privacy Browser
Summary:A web browser that respects your privacy
Description:
Privacy Browser protects your privacy by giving you easy control over the use of
JavaScript, DOM storage, and cookies. Many websites use these technologies to
track users between visits and between sites.

Privacy Browser uses Android's builtin WebView to render web pages. As such, it
works best if you have updated to the latest version of WebView.

As a nice side benefit, when JavaScript is disabled web pages load much faster
with less bandwidth consumption.

Tapping the privacy eye toggles whether JavaScript is enabled and reloads the
WebView.  The privacy eye icon also changes to indicate the current privacy
state.

All forward and back history is cleared whenever the app is closed by swiping
from the recents list, rebooting the Android device, or selecting the Clear and
Exit option from the menu.  In addition, Clear and Exit clears all DOM storage,
cookies, and destroys the WebView.
.

Repo Type:git
Repo:git://git.stoutner.com/git/PrivacyBrowser.git

Build:1.3-standard,4
    commit=v1.3-standard
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.4-standard,5
    commit=v1.4-standard
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.5,6
    commit=v1.5
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.6,7
    commit=v1.6
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.7,8
    commit=v1.7
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.8,9
    commit=v1.8
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:1.9,10
    commit=v1.9
    subdir=app
    gradle=standard
    prebuild=sed -i -e '/play-services/d' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.9
Current Version Code:10
