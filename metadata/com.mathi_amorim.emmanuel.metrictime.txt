Categories:Time
License:MIT
Web Site:
Source Code:https://github.com/mannyamorim/metrictime
Issue Tracker:https://github.com/mannyamorim/metrictime/issues

Auto Name:Metric Time
Summary:Shows the current metric time in a widget on the home screen
Description:
This app provides a widget for your home screen that shows the current time in
Metric Time (Decimal Time). You can read more about this time format here
[https://en.wikipedia.org/wiki/Decimal_time].
.

Repo Type:git
Repo:https://github.com/mannyamorim/metrictime.git

Build:0.0.1,1
    commit=v0.0.1
    subdir=app
    gradle=yes

Build:0.0.2,2
    commit=v0.0.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.0.2
Current Version Code:2
